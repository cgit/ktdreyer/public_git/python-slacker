#!/bin/bash

set -e

VERSION=0.5.4

GITHUBURL=https://github.com/os/slacker/archive/v${VERSION}.zip

# download zipball
if [[ ! -f slacker-$VERSION.zip ]]; then
    curl -o slacker-$VERSION.zip -L $GITHUBURL
fi

# extract zipball
[[ -d slacker-$VERSION ]] && rm -r slacker-$VERSION
unzip slacker-$VERSION.zip

pushd slacker-$VERSION
  # repack
  tar -cJvf slacker-$VERSION-tests.tar.xz tests
  mv slacker-$VERSION-tests.tar.xz ..
popd

# Clean up
rm slacker-$VERSION.zip
rm -r slacker-$VERSION
