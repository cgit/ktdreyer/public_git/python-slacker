%global srcname slacker

%if 0%{?fedora}
%global with_python3 1
%endif

Name:           python-%{srcname}
Version:        0.5.4
Release:        1%{?dist}
Summary:        Slack API client

License:        ASL 2.0
URL:            http://pypi.python.org/pypi/%{srcname}/
Source0:        http://pypi.python.org/packages/source/s/%{srcname}/%{srcname}-%{version}.tar.gz
Source1:        %{name}-generate-tests-tarball.sh
# To get the tarball in Source2, ensure that the upstream version number is
# correct in the "generate tarball" script above, and then run the script.
Source2:        %{srcname}-%{version}-tests.tar.xz
BuildArch:      noarch

Requires:       python-requests

BuildRequires:  python2-devel
BuildRequires:  python-setuptools
BuildRequires:  pytest
BuildRequires:  python-mock
BuildRequires:  python-requests

%description
Slacker is a full-featured Python interface for the Slack API.

%if 0%{?with_python3}
%package -n python3-%{srcname}
Summary:        Slack API client
Requires:       python3-requests
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-pytest
BuildRequires:  python3-mock
BuildRequires:  python3-requests

%description -n python3-%{srcname}
Slacker is a full-featured Python interface for the Slack API.
%endif

%prep
%setup -q -n %{srcname}-%{version} -a 2

%if 0%{?with_python3}
rm -rf %{py3dir}
cp -a . %{py3dir}
%endif # with_python3


%build
%{__python2} setup.py build

%if 0%{?with_python3}
pushd %{py3dir}
%{__python3} setup.py build
popd
%endif


%install
%{__python2} setup.py install --skip-build --root %{buildroot}

%if 0%{?with_python3}
pushd %{py3dir}
%{__python3} setup.py install --skip-build --root %{buildroot}
popd
%endif

%check
export PYTHONPATH=$(pwd)

py.test-%{python_version} -v tests

%if 0%{?with_python3}
pushd %{py3dir}
py.test-%{python3_version} -v tests
popd
%endif # with_python3


%files
%doc README.rst
%{python2_sitelib}/%{srcname}/
%{python2_sitelib}/%{srcname}*.egg-info

%if 0%{?with_python3}
%files -n python3-%{srcname}
%doc README.rst
%{python3_sitelib}/%{srcname}/
%{python3_sitelib}/%{srcname}*.egg-info
%endif

%changelog
* Fri Jan 30 2015 Ken Dreyer <ktdreyer@ktdreyer.com> - 0.5.4-1
- initial package
